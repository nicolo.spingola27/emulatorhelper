#!/usr/bin/env python3
import collections
import os
import subprocess
import sys
import yaml


def build_commands(settings):
    commands = collections.OrderedDict()
    games, emulator_settings = (settings["giochi"],
                                settings["impostazioni_emulatori"])
    rom_folder = os.path.expanduser(settings["cartella_rom"])
    for game in games:
        game_extension = os.path.splitext(game["rom"])[1][1:]
        game_program = game.get("program",
                                settings["emulatori_per_tipo"][game_extension])
        if game_program is None:
            continue
        game_command = [game_program]
        for k, v in emulator_settings.get(game_program, {}).items():
            hyphen = "-" if len(str(k)) == 1 else "--"
            game_command.append(hyphen + str(k))
            if v is not None:
                game_command.append(str(v))
        game_command.append(os.path.join(rom_folder,
                                         game["rom"]))
        commands[game["nome"]] = game_command
    return commands


def choose_game(games):
    commands = build_commands(games)
    chosen_game = subprocess.Popen(["rofi", "-dmenu"],
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE)
    game_names = "\n".join(commands.keys())
    stdout = chosen_game.communicate(game_names.encode("utf-8"))[0]
    if stdout:
        return commands[stdout.strip().decode("utf-8")]
    else:
        return None


def main():
    xrdg_config_home = os.environ.get("XDG_CONFIG_HOME",
                                      os.path.join(os.path.expanduser("~"),
                                                   ".config"))
    settings_path = os.path.join(xrdg_config_home,
                                 "emulator_helper",
                                 "impostazioni.yaml")
    with open(settings_path) as games_file:
        games = yaml.safe_load(games_file)
    __import__("pprint").pprint(games)
    chosen_game = choose_game(games)
    print(chosen_game)
    if chosen_game is not None:
        sys.exit(subprocess.Popen(chosen_game).wait())
    else:
        sys.exit(1)


if __name__ == "__main__":
    main()
