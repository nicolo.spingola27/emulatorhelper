EmulatorHelper
==============

EmulatorHelper è un applicazione fatta per aiutare l'organizzazzione dei
giochi emulati, come ad esempio quelli per il Super Nintendo.

I giochi vanno aggiunti in una apposita cartella per le ROM, e dopodiche vanno
aggiunti in un file YAML.

Spiegazione File YAML
---------------------

La chiave `cartella_rom` è la posizione **assoluta** della cartella dove si
trovano le ROM.

La chiave `impostazioni_emulatori` dà le varie impostazioni per gli emulatori,
e segue il formato

```yaml
nome_emulatore:
    impostazione_con_valore: valore
    impostazione_senza_valore: ~
```

La chiave `emulatori_per_tipo` associa gli emulatori ai tipi di file, e segue
il formato

```yaml
nome_emulatore_con_un_solo_tipo: "estensione"
nome_emulatore_con_tanti_tipi:
    - "estensione1"
    - "estensione2"
    - "estensione3"
```

Infine, la chiave `giochi` è una lista dei vari giochi (come dice il nome), e
segue il formato

```yaml
-
    nome: "Banana Simulator"
    rom: "BananaSimulator.sfc"
-
    nome: "Io Sono Una Patata"
    rom: "IoSonoUnaPatata.gba"
```

Esempio Configurazione YAML
---------------------------

```yaml
---

cartella_rom: "~/.rom"

impostazioni_emulatori:
    zsnes:
        v: 11  # 800x600 tutto-schermo
    vba:
        4: ~   # Video x4

emulatori_per_tipo:
    gba: "vba"
    sfc: "zsnes"
    n64: "mupen64plus"

giochi:
    -
        nome: "Banana Simulator"
        rom: "BananaSimulator.sfc"
    -
        nome: "Io Sono Una Patata"
        rom: "IoSonoUnaPatata.gba"
```

Installazione (su Ubuntu Linux)
-------------------------------

1. Scarica questo repositorio (`git clone`/zip/tar)
2. Vai nella cartella dell'repositorio (`cd emulatorhelper`)
3. Scarica rofi e Python 3 (`sudo apt-get install -y rofi python3-pip`)
4. Scarica i requisiti (`python3 -m pip install --user -r requirements.txt`)
5. Crea la cartella `emulator_helper` in `~/.config` (`mkdir -p
   ~/.config/emulator_helper`)

Dopodiche, per fare partire il menu dei giochi basta eseguire il comando
`python3 emulator_helper.py`

Per l'installazione su altre distribuzioni di GNU/Linux l'unico passaggio che
cambia è il secondo, dove bisogna usare il package manager della propria
distribuzione (e.g. `yum` su Arch Linux)
